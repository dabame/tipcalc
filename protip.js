// Tip calculator objject
var proTip =
{
	total: 0,
	tip: 0,
	selectedButton: "",
	calculateTip: function() // calculates the tip amount based on total
	{
		if (isNaN(this.total))
		{
			this.tip = 0;
		}
		else
		{
			switch (this.selectedButton)
			{
				case ("good"): this.tip = this.total * .15; break;
				case ("better"): this.tip = this.total * .18; break;
				case ("best"): this.tip = this.total * .20; break;
				default: break;
			}
		}
		return;
	},
	buttonPush: function(buttonId) // call when button is pushed
	{
		this.total = document.getElementById("total").value;
		this.selectedButton = buttonId;
		this.calculateTip();

		document.getElementById("tip").value = this.tip.toFixed(2);
	}
}
